package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")

public class User {
    //Primary Key
    @Id
    @GeneratedValue
    private Long id;

    //Create Columns
    @Column
    private String username;
    @Column
    private String password;

    //represents one side of relationship
    @OneToMany(mappedBy = "user")//automatically joins user with post (auto-map who is the owner of the post)
    @JsonIgnore//infinite recursion
    private Set<Post> posts;
    //Default Constructors
    public User(){}

    //Parameterized Constructors
    public User (String username, String password){
        this.username = username;
        this.password = password;
    }

    public Long getId(){
        return id;
    }

    //Get Function
    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }

    //Set Function
    public void setUsername(String username){
        this.username = username;
    }
    public void setPassword(String password){
        this.password = password;
    }

    //getting the post
    public Set<Post> getPosts(){
        return posts;
    }
}
