package com.zuitt.wdc044.models;

import javax.persistence.*;
//"@Entity" marks this object as a representation of database table
@Entity

//"@Table" designate the table name
@Table(name = "posts")

public class Post {
    //"@Id" Indicates that this property is a primary key.
    @Id

    //"@GeneratedValue" for auto-increment
    @GeneratedValue
    private Long id;

    //Declares table columns -------
    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    //Default Constructors, this is needed when retrieving posts
    public  Post(){}

    //Parameterized Constructor
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    //Set/Get Functions
    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }
}
