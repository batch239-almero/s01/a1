package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
// "@SpringBootApplication" this is called as "Annotations" mark.
// Annotations are used to provide supplemental information about the program.
// These are used to manage and configure the behavior of the framework.
// Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.

@RestController
//Tells the spring boot that this will handle endpoints for web requests
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	//"hello" endpoint
	@GetMapping("/hello")
	//@RequestParam is used to extract query parameters
		//name = john; Hello john.
		//name = World; Hello World.

	//To append the url with a name parameter, we do the following:
		//http:localhost:8080/hello?name=john
	public  String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s", name); //"%s" is format specifier
	}

	//ACTIVITY
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("hi %s!", name);
	}

	//STRETCH GOALS
	@GetMapping("/nameAge")
	public String nameAge(
			@RequestParam (value = "name", defaultValue = "World") String name,
			@RequestParam (value = "age", defaultValue = "0") String age
			){
		return String.format("Hello "+name+" ! Your age is "+age);
	}

}
