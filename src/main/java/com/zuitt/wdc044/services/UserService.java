package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;

import java.util.Optional;

//This interface will be used to register a user via userController.
public interface UserService {
    //User registration
    void createUser(User user);

    //Check if user is already existing
    Optional<User> findByUsername(String username);
}
