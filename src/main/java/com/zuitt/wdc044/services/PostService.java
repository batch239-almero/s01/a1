package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    //CREATE a post
    void createPost(String stringToken, Post post);

    //READ all posts
    Iterable<Post> getPosts();

    //EDIT a user post
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    //DELETE a user post
    ResponseEntity deletePost(Long id, String stringToken);

    //ACTIVITY --------------------------------------------
    // READ all post of logged user
    Iterable<Post> getMyPosts(String stringToken);

}
