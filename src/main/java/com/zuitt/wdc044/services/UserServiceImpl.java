package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service //annotation is used to indicate that it holds the actual business logic
public class UserServiceImpl implements UserService {

    @Autowired // autowired is used to access object and methods of another class.
    private UserRepository userRepository; //To be able to use the CrudRepository

    //Create a user
    public void createUser(User user){
        userRepository.save(user);
    }

    //Check if user exists -------------------
    //Returns entity based on the given criteria
    //Empty instance of the optional class
    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }
}
